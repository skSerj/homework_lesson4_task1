package com.company;

import org.w3c.dom.ls.LSOutput;

public class Main {

    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book();
        books[0].title = "Этюд в багровых тонах";
        books[0].authorName = "Артур Конан Дойл";
        books[0].yearOfPublishing = 1987;

        books[1] = new Book();
        books[1].title = "Чувство льда";
        books[1].authorName = "Александра Маринина";
        books[1].yearOfPublishing = 2006;

        books[2] = new Book();
        books[2].title = "Бесы";
        books[2].authorName = "Фёдор Достоевский";
        books[2].yearOfPublishing = 1872;

        books[3] = new Book();
        books[3].title = "Они сражались за родину";
        books[3].authorName = "Михаил Шолохов";
        books[3].yearOfPublishing = 1943;

        books[4] = new Book();
        books[4].title = "Мастер и Маргарита";
        books[4].authorName = "Михаил Булгаков";
        books[4].yearOfPublishing = 1967;


        int findYearOfPublishing = 2006;
        String findAuthor = "Михаил Шолохов";
        int maxYearOfPublishingToFind = 1990;
        int minYearOfPublishing = books[0].yearOfPublishing;

        for (Book a : books) {
            if (a.yearOfPublishing == findYearOfPublishing) {
                System.out.println("Искомая книга по году издания: " + a.title + "; Автор: " + a.authorName);
            }
        }

        for (Book i : books) {
            if (i.authorName.toUpperCase().equals(findAuthor.toUpperCase())) {
                System.out.println("Искомой книгой по автору " + findAuthor + " , является : " + i.title);
            }
        }

        for (Book b : books) {
            if (b.yearOfPublishing < minYearOfPublishing) {
                minYearOfPublishing = b.yearOfPublishing;
            }
        }
        System.out.println("самая ранняя книга со списка была издана в " + minYearOfPublishing + " году");

        for (Book c : books) {
            if (c.yearOfPublishing < maxYearOfPublishingToFind) {
                System.out.println("книга выпущенная ранне " + maxYearOfPublishingToFind + " года: " + c.title + "; автор: " + c.authorName + ", " + c.yearOfPublishing + " г.");
            }
        }
    }
}

